using Microsoft.EntityFrameworkCore;
using Moq;
using SecureFlight.Core.Entities;
using SecureFlight.Infrastructure;
using SecureFlight.Infrastructure.Repositories;
using System;
using Xunit;

namespace SecureFlight.Test
{
    public class AirportTests
    {
        private readonly BaseRepository<Airport> _repo;

        public AirportTests()
        {
            var mockSet = new Mock<DbSet<Airport>>();

            Mock<SecureFlightDbContext> context = new Mock<SecureFlightDbContext>();
            context.Setup(m => m.Airports).Returns(mockSet.Object);

            _repo = new BaseRepository<Airport>(context.Object); 
        }

        [Fact]
        public void Update_Succeeds()
        {
            //Arrange
            var airportToEdit = new Airport
            {
                City = "Test",
                Country = "Argentina",
                Code = "AAQ",
                Name = "Anapa Vityazevo"
            };

            var result = _repo.Update(airportToEdit);

            Assert.NotNull(result);
           // _repo.Verify(m => m.SaveChanges(), Times.Once());
        }
    }
}
