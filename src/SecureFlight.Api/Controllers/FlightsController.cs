﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FlightsController : ControllerBase
    {
        private readonly IService<Flight> _flightService;
        private readonly IRepository<PassengerFlight> _flightRepo;

        public FlightsController(IService<Flight> flightService, IRepository<PassengerFlight> flightRepo)
        {
            _flightService = flightService;
            _flightRepo = flightRepo;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public async Task<IActionResult> Get()
        {
            var flights = (await _flightService.GetAllAsync()).Result
                .Select(x => new FlightDataTransferObject
                {
                    Id = x.Id,
                    ArrivalDateTime = x.ArrivalDateTime,
                    Code = x.Code,
                    FlightStatusId = (int) x.FlightStatusId,
                    DepartureDateTime = x.DepartureDateTime,
                    DestinationAirport = x.DestinationAirport,
                    OriginAirport = x.OriginAirport
                });

            return Ok(flights);
        }

        [HttpGet]
        [Route("GetByDestination/{destination}/{origin}")]
        [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public async Task<IActionResult> GetByDestinationAndOrigin(string destination, string origin)
        {
            if (String.IsNullOrEmpty(destination) || String.IsNullOrEmpty(origin))
                return BadRequest("Missing parameters");

            var flights = (await _flightService.GetAllAsync()).Result
                .Select(x => new FlightDataTransferObject
                {
                    Id = x.Id,
                    ArrivalDateTime = x.ArrivalDateTime,
                    Code = x.Code,
                    FlightStatusId = (int)x.FlightStatusId,
                    DepartureDateTime = x.DepartureDateTime,
                    DestinationAirport = x.DestinationAirport,
                    OriginAirport = x.OriginAirport
                })
                .Where(x => x.DestinationAirport.Equals(destination) && x.OriginAirport.Equals(origin));

            return Ok(flights);
        }


        [HttpPost]
        [Route("AddPassenger/{passenger}/{flightNumber}")]
        [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public async Task<IActionResult> GetByDestinationAndOrigin(string passenger, long flightNumber)
        {
            if (passenger == null)
                return BadRequest("Missing parameters");

            var passengerAdded = _flightRepo.Add(new PassengerFlight()
            {
                FlightId = flightNumber,
                PassengerId = passenger
            });

            return Ok($"Passenger added succesfully {passenger}");
        }
    }
}
